
var gl;
var program;

function main() {

///////////////////////////////////////// My variables

	var vertexShaderString = document.getElementById("VERTEX_SHADER").textContent;
	var fragmentShaderString = document.getElementById("FRAGMENT_SHADER").textContent;	
	var canvas = document.getElementById("glCanvas");

/////////////////////////////////////////// Initially resizing my canvas

	canvas.width = window.innerWidth - 15;
	canvas.style.width = canvas.width
	
	canvas.height = window.innerHeight - 16;
	canvas.style.height = canvas.height;


///////////////////////////////////////////////////Getting the webGL context so javascript knows what i am talking about

	gl = canvas.getContext("webgl");
	
	if (gl === null) {
	
		alert("Unable to initialize WebGL. Your browser or machine may not support it.");
		return;

	}







////////////////////////////////////////////////////////////////////////////// Defining the 2D vertex positions for my fullscreen quad

	var screenQuadVerts = [ -1,-1,   1,-1,  -1, 1,   1,-1,   1, 1,  -1, 1 ];
				  var uv = [  0, 0,   0, 1,   1, 0,   1, 0,   0, 1,   1, 1 ];

///////////////////////////////////////////////////////////////////////// Creating an Array-buffer for the vertex positions of my quad	

	const vertexPosBuffer = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vertexPosBuffer);
	gl.bufferData( gl.ARRAY_BUFFER, new Float32Array(screenQuadVerts), gl.DYNAMIC_DRAW);

////////////////////////////////////////////////////////////////////////// Creating webGL shaders from the contents of my script-elements

	var vertexShader = gl.createShader(gl.VERTEX_SHADER);
	var fragmentShader = gl.createShader(gl.FRAGMENT_SHADER);

	gl.shaderSource(vertexShader, vertexShaderString);
	gl.shaderSource(fragmentShader, fragmentShaderString);
	







//////////////////////////////////////////////////////////////////////////// Compiling shaders

	gl.compileShader(vertexShader);
	if (!gl.getShaderParameter(vertexShader, gl.COMPILE_STATUS)) {
	
		console.error("Error compiling vertexShader: ", gl.getShaderInfoLog(vertexShader));

	}

	gl.compileShader(fragmentShader);
	if (!gl.getShaderParameter(fragmentShader, gl.COMPILE_STATUS)) {
	
		console.error("Error compiling fragmentShader: ", gl.getShaderInfoLog(fragmentShader));
	
	}

///////////////////////////////////////////////////////////////////////// Creating the combined shader program from vertex and fragmentshader...
	
	program = gl.createProgram();
	gl.attachShader(program, vertexShader);
	gl.attachShader(program, fragmentShader);
	gl.linkProgram(program);








/////////////////////////////////////////////////////////////////////// Setting up vertex-attributes and using program

	var positionLocation = gl.getAttribLocation(program, "position");
	gl.enableVertexAttribArray(positionLocation);
	gl.vertexAttribPointer(positionLocation, 2, gl.FLOAT, false, 0, 0);

	gl.useProgram(program);





/////////////////////////////////////////////////////////////////////////////////// Do interesting stuff here

	gl.uniform1f(gl.getUniformLocation(program, "SCREEN_WIDTH"), canvas.width);
	gl.uniform1f(gl.getUniformLocation(program, "SCREEN_HEIGHT"), canvas.height);
	
/////////////////////////////////////////////////////////////////////////////////// Stop doing interesting stuff here...



		gl.drawArrays(gl.TRIANGLES, 0, 6);


}



function setCamPos(gl, program, x, y, z) {

	gl.uniform3f(gl.getUniformLocation(program, "CAM_POS"), x, y, z);

}

function getGL() {

	return gl;

}

function getProgram() {

	return program;

}

//////////////////////////// Executinc main funciton when window loads...

window.onload = main;




















